const http = require('http');

module.exports = app => {
  if (process.env.NODE_ENV !== 'test') {
    http.createServer(app).listen(app.config.variables.port, () => {
      app.services.publish.start();
      console.log(`http://localhost:${app.config.variables.port}`);
    });
  }
};

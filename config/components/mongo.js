const mongoose = require('mongoose');

module.exports = app => {
  mongoose.connect(
    `mongodb://${app.config.variables.hostMongo}/${
      app.config.variables.dataBase
    }`,
    {useNewUrlParser: true}
  );
  return {
    mongoose
  };
};

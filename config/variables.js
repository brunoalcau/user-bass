module.exports = app => ({
  port: process.env.PORT || 8080,
  hostMongo: process.env.HOST_MONGO || 'localhost:27017',
  dataBase: process.env.DATA_BASE || 'user-management',
  kongUrl: process.env.KONG_URL || 'http://localhost:8001',
  rabbitPort: process.env.RABBIT_PORT || 5672,
  rabbitUser: process.env.RABBIT_USER || 'user',
  rabbitPassword: process.env.RABBIT_PASSWORD || 'bitnami',
  rabbitHost: process.env.RABBIT_HOST || 'localhost',
  rabbitQueue: process.env.RABBIT_QUEUE || 'opsico-request-email',
  rabbitExchange: process.env.RABBIT_EXCHANGE || 'opsico-email',
  emailHost: process.env.EMAIL_HOST || 'smtp.mailtrap.io',
  facebookId: process.env.FACEBOOK_ID || '159557788148456',
  googleApikey:
    process.env.GOOGLE_API_KEY || 'AIzaSyBDpG8Zlmv0iyxLwXAJezL3F_YQTjZ-ZT8',
  facebookScrete:
    process.env.FACEBOOK_SCRETE || '3068bccc3f5180f664da7f1dc7f1c910'
});

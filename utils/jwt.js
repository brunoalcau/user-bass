const atob = require('atob');

module.exports = app => ({
  decode: code => {
    if (!code) {
      throw new Erro('code not definition');
    }
    const splitBearer = code.split(' ');
    if (!splitBearer.length) {
      throw new Erro('invalid jwt');
    }
    const codes = splitBearer[1].split('.');
    return JSON.parse(atob(codes[1]));
  }
});

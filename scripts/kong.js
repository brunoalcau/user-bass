
curl -i -X POST \
  --url http://localhost:8001/apis/ \
  --data 'name=example-api' \
  --data 'hosts=example.com' \
  --data 'upstream_url=http://mockbin.org'
Permalink


{
"total": 2,
"data": [
{
"created_at": 1535114873207,
"strip_uri": true,
"id": "88a1be0b-08a3-4dff-b1a6-393e0afa2b1a",
"hosts": [
"localhost"
],
"name": "api-user",
"upstream_url": "http://10.101.10.97:8082/user/",
"http_if_terminated": true,
"https_only": false,
"retries": 5,
"uris": [
  "/user"
],
"upstream_send_timeout": 60000,
"upstream_connect_timeout": 60000,
"upstream_read_timeout": 60000,
"methods": [
"GET",
"POST",
"PUT",
"DELETE",
"OPTIONS"
],
"preserve_host": false
},
{
"created_at": 1535116201409,
"strip_uri": true,
"id": "05357941-b892-49db-a357-e5b1a557cd62",
"hosts": [
"localhost"
],
"name": "api-user-offiline",
"upstream_url": "http://10.101.10.97:8082/offline/",
"http_if_terminated": true,
"https_only": false,
"retries": 5,
"uris": [
"/offline"
],
"upstream_send_timeout": 60000,
"upstream_connect_timeout": 60000,
"upstream_read_timeout": 60000,
"methods": [
"GET",
"POST",
"PUT",
"DELETE",
"OPTIONS"
],
"preserve_host": false
}
]
}
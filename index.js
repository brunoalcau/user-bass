require('dotenv').config();
const consign = require('consign');
const app = require('express')();
consign({verbose: true})
  .include('config/variables.js')
  .then('config/components/server.js')
  .then('config/components/mongo.js')
  .then('utils')
  .then('models/mongo')
  .then('repositories')
  .then('services')
  .then('web/middleware/passport.js')
  .then('web/middleware/apiRest.js')
  .then('web/controllers')
  .then('web/router/api')
  .into(app);

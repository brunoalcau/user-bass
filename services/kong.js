const jwt = require('jsonwebtoken');
const axios = require('axios');

module.exports = app => ({
  createJwtKong: async (apikey, user, expiresIn = '30d') => {
    try {
      const {kongUrl} = app.config.variables;
      const {getConsumer} = app.services.kong;
      const consumer = await getConsumer(apikey);
      const {data} = await axios.post(
        `${kongUrl}/consumers/${consumer.id}/jwt`
      );
      const token = jwt.sign(
        {id: user.id, email: user.email, type: user.type, iss: data.key},
        data.secret,
        {
          algorithm: 'HS256',
          expiresIn: expiresIn
        }
      );
      return token;
    } catch (e) {
      throw new Error(e.message);
    }
  },
  getConsumer: async apikey => {
    try {
      const {kongUrl} = app.config.variables;
      const {data} = await axios.get(`${kongUrl}/key-auths/${apikey}/consumer`);
      return data;
    } catch (e) {
      throw new Error('[KONG GET CONSUMER]');
    }
  },
  deleteJwtKong: async (user, apikey) => {
    try {
      const {kongUrl} = app.config.variables;
      const {getConsumer} = app.services.kong;
      const consumer = await getConsumer(apikey);
      await axios.delete(`${kongUrl}/consumers/${consumer.id}/jwt/${user.iss}`);
    } catch (e) {
      throw new Error(e.message);
    }
  }
});

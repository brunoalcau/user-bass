const axios = require('axios');
const {keys, map, head} = require('lodash');
const replaceAll = require('../utils/replaceAll');
// const {head, keys, map} = require('lodash');

module.exports = app => ({
  getLocation: async address => {
    try {
      const adressKeys = keys(address);

      const adressFull = map(adressKeys, item => address[item]).toString();
      const url = `https://maps.googleapis.com/maps/api/geocode/json`;
      const {data} = await axios.get(url, {
        params: {
          key: app.config.variables.googleApikey,
          address: adressFull
        },
        headers: {
          'Content-Type': 'application/json; charset=UTF-8'
        }
      });
      const result = head(data.results);
      if (result) {
        const location = result.geometry.location;
        const propertyKeys = keys(location);
        const latLong = map(propertyKeys, item => location[item]);
        return {
          type: 'Point',
          coordinates: latLong
        };
      } else {
        return {
          type: 'Point',
          coordinates: []
        };
      }
    } catch (e) {
      console.log(e);
    }
  }
});

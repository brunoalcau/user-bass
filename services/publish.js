// const amqp = require('amqplib/callback_api');
let channel = null;

module.exports = app => ({
  start: async () => {
    const amqpConn = await app.services.connect.connect();
    try {
      channel = await amqpConn.createConfirmChannel();
      return channel;
    } catch (e) {
      amqpConn.close();
      console.error('[AMQP] error', e);
    }
  },
  publish: async (exchange, routingKey, content) => {
    try {
      await channel.assertQueue('email-register-response', {
        durable: true
      });
      channel.publish(
        exchange,
        routingKey,
        content,
        {
          persistent: true,
          replyTo: 'email-register-response'
        },
        (err, ok) => {
          if (err) {
            channel.connection.close();
          }
        }
      );
    } catch (e) {
      console.error('[AMQP] publish catch', e.message);
    }
  }
});

const amqp = require('amqplib');

module.exports = app => ({
  connect: async () => {
    try {
      const {
        rabbitUser,
        rabbitPassword,
        rabbitHost,
        rabbitPort
      } = app.config.variables;
      const stringConnect = `amqp://${rabbitUser}:${rabbitPassword}@${rabbitHost}:${rabbitPort}`;
      const conn = await amqp.connect(stringConnect);
      return conn;
    } catch (e) {
      throw Error(e.message);
    }
  }
});

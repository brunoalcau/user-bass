module.exports = app => ({
  registeredUser: user => {
    const {rabbitQueue} = app.config.variables;
    app.services.publish.publish(
      '',
      rabbitQueue,
      new Buffer(
        JSON.stringify({email: user, emailKey: user.id, type: 'register'})
      )
    );
  }
});

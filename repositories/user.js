const {isEmpty, map, orderBy} = require('lodash');
const {ObjectId} = require('mongoose').Types;
module.exports = app => ({
  create: user => {
    const User = new app.models.mongo.user.db(user);
    return User.save();
  },
  login: async user => {
    try {
      const responseUser = await app.models.mongo.user.db
        .findOne({
          email: user.email.toLowerCase()
        })
        .select('+password');
      const isValid = responseUser
        ? await responseUser.comparePassword(user.password)
        : false;
      return isValid ? responseUser : null;
    } catch (e) {
      throw new Error(e.message);
    }
  },
  // TODO: Melhorar os codigos
  loginFacebook: async user => {
    const responseUser = await app.models.mongo.user.db.findOne({
      $and: [{email: user.email.toLowerCase()}]
    });
    if (responseUser) {
      await app.models.mongo.user.db.update(
        {email: user.email},
        {$set: {accessToken: user.accessToken}}
      );
      // Não criar object do nada remover
      return {
        name: responseUser.name,
        email: responseUser.email,
        type: responseUser.type,
        urlImage: responseUser.urlImage,
        isCreate: false
      };
    } else {
      const User = new app.models.mongo.user.db(user);
      const userResponse = await User.save();
      // Não criar object do nada remover
      return {
        name: userResponse.name,
        email: userResponse.email,
        type: userResponse.type,
        urlImage: userResponse.urlImage,
        isCreate: true
      };
    }
  },
  resetPassword: async (user, passwords) => {
    // const responseUser = await app.models.mongo.user.db
    //   .findOne({_id: user.id})
    //   .select('+password');
    // console.log(responseUser);
    // const isValid = responseUser
    //   ? await responseUser.comparePassword(passwords.current)
    //   : false;
    // if (!isValid) {
    //   throw new Error('Senha atual não confere!');
    // }

    await app.models.mongo.user.db.update(
      {email: user.email},
      {$set: {password: passwords.new}}
    );
  },
  getDoctors: async doctorId => {
    const address = await app.models.mongo.address.db
      .findOne({user: doctorId})
      .select('-user -_id -__v -location');

    if (isEmpty(address)) {
      return null;
    }

    const dayOffer = await app.models.mongo.dayOffer.db.find({
      user: ObjectId(doctorId)
    });

    const week = orderBy(map(dayOffer, day => day.weekDay));

    const details = {
      street: address.street,
      district: address.district,
      number: address.number,
      zip: address.zip,
      weekOffer: week
    };
    return details;
  }
});

const {ObjectId} = require('mongoose').Types;
const {map, difference, find, isEqual, cloneDeep} = require('lodash');
module.exports = app => ({
  save: async schedule => {
    try {
      const isSchedule = await app.repositories.schedule.getSchedule(schedule);
      if (isSchedule.length) {
        throw new Error('Este horário não está mais disponivel');
      }
      const Schedule = app.models.mongo.schedule.db;
      return await Schedule.create(schedule);
    } catch (e) {
      console.log(e);
      throw new Error(e.message);
    }
  },
  getSchedule: async schedule => {
    const result = await app.models.mongo.schedule.db.find({
      date: new Date(schedule.date),
      hour: schedule.hour
    });
    return result;
  },
  getByDate: async (date, doctor) => {
    try {
      const result = await app.models.mongo.schedule.db.find({
        doctor: ObjectId(doctor),
        date: new Date(date),
        status: 'aprovate'
      });
      const scheduleDay = map(result, item => item.hour);

      const day = moment(date).day();
      const dayOffer = await app.models.mongo.dayOffer.db.findOne({
        user: ObjectId(doctor),
        weekDay: day
      });
      return difference(dayOffer ? dayOffer.hours : [], scheduleDay);
    } catch (e) {
      console.log(e.message);
      throw new Error(e.message);
    }
  },
  getSchedulesByPatient: async patient => {
    const schedule = await app.models.mongo.schedule.db
      .find({
        patient: ObjectId(patient)
      })
      .populate('doctor');
    return schedule;
  },
  updateStatusSchedule: async (scheduleId, type) => {
    const shedule = await app.models.mongo.schedule.db.update(
      {_id: ObjectId(scheduleId)},
      {$set: {status: type}}
    );
  }
});

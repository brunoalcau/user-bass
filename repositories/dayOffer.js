module.exports = app => ({
  save: async dayOffer => {
    const DayOffer = app.models.mongo.dayOffer.db;
    const dias = await DayOffer.create(dayOffer);
    return dias;
  }
});

const {ObjectId} = require('mongoose').Types;
const {map, reduce} = require('lodash');
module.exports = app => ({
  scheduleSave: async schedule => {
    const SchedulingAmount = app.models.mongo.schedulingAmount.db;
    return await SchedulingAmount.create(schedule);
  },
  getAmount: async userId => {
    try {
      const schedulingAmount = await app.models.mongo.schedulingAmount.db.find({
        patient: ObjectId(userId)
      });
      const quantities = map(schedulingAmount, item => item.quantity);
      const quantity = reduce(
        quantities,
        (sum, n) => {
          if (sum < 0) {
            return sum - n;
          } else {
            return sum + n;
          }
        },
        0
      );
      return quantity;
    } catch (e) {
      throw new Error(e.message);
    }
  }
});

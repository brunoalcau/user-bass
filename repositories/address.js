const {ObjectId} = require('mongoose').Types;
module.exports = app => ({
  save: address => {
    const Address = new app.models.mongo.address.db(address);
    return Address.save();
  },
  get: async () => {
    const address = await app.models.mongo.address.db
      .find()
      .populate('user', 'urlImage name');
    return address;
  },
  findById: async id => {
    const address = await app.models.mongo.address.db.findOne({
      user: ObjectId(id)
    });
    return address;
  }
});

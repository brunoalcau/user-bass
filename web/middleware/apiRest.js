const helmet = require('helmet');
const jwt = require('../../utils/jwt');
const compression = require('compression');
const methodOverride = require('method-override');
const bodyParser = require('body-parser');
const passport = require('passport');
const FacebookTokenStrategy = require('passport-facebook-token');
const multer = require('multer');
const cors = require('cors');
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, `uploads/`);
  },
  filename: (req, file, cb) => {
    const authorization = req.headers['authorization'];
    const user = jwt().decode(authorization);
    cb(null, `${user.id}-${Date.now()}.jpg`);
  }
});
const upload = multer({storage: storage});

module.exports = app => {
  app.set('port', app.config.variables || 8081);
  app.set('json spaces', 4);
  app.use(cors());
  app.use(
    bodyParser.urlencoded({
      extended: true
    })
  );
  app.use(upload.array('documents', 3));
  app.web.middleware.passport.getPassport().authenticate('facebook-token');
  app.use(bodyParser.json());
  app.use(methodOverride());
  app.use(helmet());
  app.use(compression());
};

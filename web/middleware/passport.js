const passport = require('passport');
const FacebookTokenStrategy = require('passport-facebook-token');
const {first} = require('lodash');
module.exports = app => ({
  getPassport: () => {
    passport.use(
      new FacebookTokenStrategy(
        {
          clientID: app.config.variables.facebookId,
          clientSecret: app.config.variables.facebookScrete
        },
        (accessToken, refreshToken, profile, done) => {
          done(null, {
            id: profile._json.id,
            name: profile._json.name,
            email: profile._json.email,
            urlImage: first(profile.photos)
              ? first(profile.photos).value
              : null,
            accessToken
          });
        }
      )
    );
    passport.serializeUser(function(user, done) {
      done(null, user);
    });

    passport.deserializeUser(function(obj, done) {
      done(null, obj);
    });
    app.use(passport.initialize());
    return passport;
  }
});

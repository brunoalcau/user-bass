module.exports = app => {
  app.route('/user/adress/v1/create').post(async (req, res) => {
    try {
      const apikey = req.headers['authorization'];
      const {save} = app.web.controllers.address;
      const address = await save(apikey, req.body);
      res.status(200).json({data: address});
    } catch (e) {
      res.status(500).json({error: e.message});
    }
  });
};

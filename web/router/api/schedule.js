module.exports = app => {
  app.route('/doctors/schedule').post(async (req, res) => {
    try {
      const apikey = req.headers['authorization'];
      const {save} = app.web.controllers.schedule;
      const schedule = await save(apikey, req.body);
      res.status(200).json({data: schedule});
    } catch (e) {
      res.status(500).json({error: e.message});
    }
  });
  app.route('/doctors/:doctor/schedule/:date').get(async (req, res) => {
    try {
      const apikey = req.headers['authorization'];
      const {getByDate} = app.web.controllers.schedule;
      const dates = await getByDate(apikey, req.params.date, req.params.doctor);
      res.status(200).json({data: dates});
    } catch (e) {
      res.status(500).json({error: e.message});
    }
  });
  app.route('/doctors/schedules').get(async (req, res) => {
    try {
      const authorization = req.headers['authorization'];
      const {getSchedulesByPatient} = app.web.controllers.schedule;
      const dates = await getSchedulesByPatient(authorization);
      res.status(200).json({data: dates});
    } catch (e) {
      res.status(500).json({error: e.message});
    }
  });
  app.route('/doctors/schedules/cancel').post(async (req, res) => {
    try {
      const authorization = req.headers['authorization'];
      const {scheduleCancel} = app.web.controllers.schedule;
      await scheduleCancel(authorization, req.body);
      res.status(200).json({data: 'ok'});
    } catch (e) {
      res.status(500).json({error: e.message});
    }
  });
};

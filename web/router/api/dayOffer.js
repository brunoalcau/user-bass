module.exports = app => {
  app.route('/doctors/days').post(async (req, res) => {
    try {
      const apikey = req.headers['authorization'];
      const {save} = app.web.controllers.dayOffer;
      const days = await save(apikey, req.body.offers);
      res.status(200).json({data: days});
    } catch (e) {
      res.status(500).json({error: e.message});
    }
  });
};

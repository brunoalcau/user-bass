module.exports = app => {
  app.route('/user/v1/login').get(async (req, res) => {
    try {
      res.status(200).json({status: 'ok-test'});
    } catch (e) {
      console.log(e);
      res.status(500).json({status: 'off'});
    }
  });

  app.route('/user/v1').get(async (req, res) => {
    try {
      res.status(200).json({status: 'ok'});
    } catch (e) {
      console.log(e);
      res.status(500).json({status: 'off'});
    }
  });

  app.route('/offline/v1').get(async (req, res) => {
    try {
      res.status(200).json({status: 'ok'});
    } catch (e) {
      console.log(e);
      res.status(500).json({status: 'off'});
    }
  });

  app.route('/user/v1/login').post(async (req, res) => {
    try {
      const apikey = req.headers['apikey'];
      const user = await app.web.controllers.user.login(req.body, apikey);
      if (user) {
        res.status(200).json({data: user});
      } else {
        res.status(401).json({error: 'Unauthorized'});
      }
    } catch (e) {
      res.status(500).json({error: e.message});
    }
  });
  app
    .route('/user/v1/loginfacebook')
    .post(
      app.web.middleware.passport.getPassport().authenticate('facebook-token'),
      async (req, res) => {
        try {
          const {loginFacebook} = app.web.controllers.user;
          const apikey = req.headers['apikey'];
          const response = await loginFacebook(req.user, apikey);
          if (response) res.status(200).json({data: response});
        } catch (e) {
          console.log(e);
          res.status(500).json({error: e.message});
        }
      }
    );

  app.route('/offline/v1/logout').post(async (req, res) => {
    try {
      const authorization = req.headers['authorization'];
      const apikey = req.headers['apikey'];
      await app.web.controllers.user.logout(authorization, apikey);
      res.status(200).json({status: 'No Content'});
    } catch (e) {
      res.status(500).json({error: e.message});
    }
  });
  app.route('/offline/v1/resetpassword').post(async (req, res) => {
    try {
      const authorization = req.headers['authorization'];
      const passwords = req.body;
      await app.web.controllers.user.resetPassword(authorization, passwords);
      res.status(200).json({status: 'ok'});
    } catch (e) {
      console.log(e);
      res.status(500).json({error: e.message});
    }
  });

  app.route('/user/v1/register').post(async (req, res) => {
    try {
      const {create} = app.web.controllers.user;
      const apikey = req.headers['apikey'];
      const newUser = await create(req.body, apikey);
      res.status(200).json({data: newUser});
    } catch (e) {
      res.status(500).json({error: e.message});
    }
  });

  app.route('/user/v1/registerdoctor').post(async (req, res) => {
    try {
      const {create} = app.web.controllers.user;
      const apikey = req.headers['apikey'];
      const user = req.body;
      user.type = 'doctor';
      user.status = 'process-register-doctor';
      const newUser = await create(user, apikey);
      res.status(200).json({data: newUser});
      // res.status(200).json({status: 'ok'});
    } catch (e) {
      res.status(500).json({error: e.message});
    }
  });
  app.route('/user/v1/doctors').get(async (req, res) => {
    try {
      const {latt, long, distance} = req.query;
      const {doctors} = app.web.controllers.user;
      const responser = await doctors(latt, long, distance);
      res.status(200).json({data: responser});
    } catch (e) {
      res.status(500).json({error: e.message});
    }
  });
  app.route('/documents').post((req, res) => {
    try {
      const authorization = req.headers['authorization'];
      const files = req.files;
      const {saveDocuments} = app.web.controllers.user;
      saveDocuments(authorization, files);
      res.status(200).json({status: 'ok'});
    } catch (e) {
      res.status(500).json({status: 'off'});
    }
  });
  app.route('/doctors/:id').get(async (req, res) => {
    try {
      const {getDoctors} = app.web.controllers.user;
      const doctor = await getDoctors(req.params.id);
      if (!doctor) {
        res.status(404).json({data: 'not found'});
        return;
      }
      res.status(200).json({data: doctor});
    } catch (e) {
      res.status(500).json({error: e.message});
    }
  });
};

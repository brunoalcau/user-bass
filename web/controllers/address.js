const {map, keys} = require('lodash');
module.exports = app => ({
  save: async (authorization, address) => {
    try {
      const user = app.utils.jwt.decode(authorization);
      const {getLocation} = app.services.geocode;
      const {save} = app.repositories.address;
      const location = await getLocation(address);
      address.location = location;
      address.user = user.id;
      return save(address);
    } catch (e) {
      throw new Error(e.message);
    }
  },

  findById: async doctorId => {
    try {
      const {findById} = app.repositories.address;
      const address = await findById(doctorId);
      return address;
    } catch (e) {
      throw new Error(e.message);
    }
  }
});

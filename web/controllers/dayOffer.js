const {map, keys} = require('lodash');
module.exports = app => ({
  save: async (authorization, days) => {
    try {
      const user = app.utils.jwt.decode(authorization);
      const {save} = app.repositories.dayOffer;
      const newDays = map(days, day => ({...day, ...{user: user.id}}));
      return save(newDays);
    } catch (e) {
      throw new Error(e.message);
    }
  }
});


const {map, keys} = require('lodash');
const moment = require('moment');

module.exports = app => ({
  save: async (authorization, schedule) => {
    try {
      const user = app.utils.jwt.decode(authorization);
      const {save} = app.repositories.schedule;
      const {scheduleSave, getAmount} = app.repositories.schedulingAmount;
      const amount = await getAmount(user.id);
      if (!amount) {
        throw new Error('Seus agendamentos acabaram por favor contrate mais!');
      }
      const newSchedule = Object.assign(schedule, {
        patient: user.id
      });
      await scheduleSave({
        quantity: -1,
        patient: user.id
      });
      return save(newSchedule);
    } catch (e) {
      console.log(e);
      throw new Error(e.message);
    }
  },
  getByDate: async (authorization, date, doctor) => {
    try {
      const {getByDate} = app.repositories.schedule;
      const user = app.utils.jwt.decode(authorization);
      const dates = await getByDate(date, doctor);
      return dates;
    } catch (e) {
      throw new Error(e.message);
    }
  },
  getSchedulesByPatient: async authorization => {
    try {
      const {getSchedulesByPatient} = app.repositories.schedule;
      const user = app.utils.jwt.decode(authorization);
      const dates = await getSchedulesByPatient(user.id);
      return dates;
    } catch (e) {
      throw new Error(e.message);
    }
  },
  scheduleCancel: async (authorization, schedule) => {
    try {
      const {updateStatusSchedule} = app.repositories.schedule;
      const {get} = app.repositories.maxDayDevolution;
      const user = app.utils.jwt.decode(authorization);
      if (user.id !== schedule.patient) {
        throw new Erro('Error ao cancelamento entre em contato com suporte!');
      }
      // await updateStatusSchedule(schedule._id, 'canceled-patient');
      
      const day = await get();
      const daysDiff = moment(schedule.date).diff(moment(), 'days');
      

    } catch (e) {
      console.log(e.message);
      throw new Error(e.message);
    }
  }
});

const {omit} = require('lodash');
module.exports = app => ({
  create: async (user, apikey) => {
    try {
      const {create} = app.repositories.user;
      const {createJwtKong} = app.services.kong;
      const {registeredUser} = app.services.sendMail;
      const newUser = await create(user);
      const token = await createJwtKong(apikey, newUser);
      registeredUser(newUser);
      return {token, email: user.email, name: user.name};
    } catch (e) {
      throw new Error(e.message);
    }
  },
  login: async (user, apikey) => {
    try {
      const {createJwtKong} = app.services.kong;
      const {login} = app.repositories.user;
      const userValid = await login(user);

      if (!userValid) {
        return null;
      }
      const token = await createJwtKong(apikey, userValid);
      // Não criar object do nada remover
      return {
        token,
        email: userValid.email,
        name: userValid.name,
        urlImage: userValid.urlImage,
        type: userValid.type,
        status: userValid.status
      };
    } catch (e) {
      throw new Error(e.message);
    }
  },
  logout: async (authorization, apikey) => {
    try {
      const {deleteJwtKong} = app.services.kong;
      const user = app.utils.jwt.decode(authorization);
      await deleteJwtKong(user, apikey);
    } catch (e) {
      throw new Error(e.message);
    }
  },
  loginFacebook: async (userFacebook, apikey) => {
    try {
      const {loginFacebook} = app.repositories.user;
      const restLoginFacebook = await loginFacebook(omit(userFacebook, ['id']));
      const {createJwtKong} = app.services.kong;
      const token = await createJwtKong(apikey, restLoginFacebook);
      const {registeredUser} = app.services.sendMail;
      if (token.isSendMail) {
        registeredUser(restLoginFacebook);
      }
      // Não criar object do nada remover
      return {
        token,
        email: restLoginFacebook.email,
        name: restLoginFacebook.name,
        urlImage: restLoginFacebook.urlImage,
        type: restLoginFacebook.type,
        status: restLoginFacebook.status
      };
    } catch (e) {
      throw new Error(e.message);
    }
  },
  resetPassword: async (authorization, passwords) => {
    try {
      const user = app.utils.jwt.decode(authorization);
      const {resetPassword} = app.repositories.user;
      await resetPassword(user, passwords);
    } catch (e) {
      throw new Error(e.message);
    }
  },
  doctors: async () => {
    try {
      const {get} = app.repositories.address;
      const doctors = await get();
      return doctors;
    } catch (e) {
      throw new Error(e.message);
    }
  },
  saveDocuments: async (authorization, files) => {
    const user = app.utils.jwt.decode(authorization);
    console.log(user);
  },
  getDoctors: async doctorId => {
    const {getDoctors} = app.repositories.user;
    const doctor = await getDoctors(doctorId);
    return doctor;
  }
});

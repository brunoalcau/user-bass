const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const MaxDayDevolution = new Schema({
  date: {type: Number, required: true},
  created: {type: Date, default: Date.now}
});

module.exports = app => {
  return {
    db: app.config.components.mongo.mongoose.model('MaxDayDevolution', MaxDayDevolution)
  };
};

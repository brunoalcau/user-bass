const mongoose = require("mongoose"),
  Schema = mongoose.Schema;

const AddressSchema = new Schema({
  street: { type: String, required: true },
  district: { type: String, required: true },
  number: { type: String, required: true },
  complement: { type: String },
  zip: { type: String, required: true },
  location: {
    type: { type: String },
    coordinates: []
  },
  user: { type: Schema.Types.ObjectId, ref: "User", required: true }
});

AddressSchema.index({ location: "2dsphere" });

module.exports = app => {
  return {
    db: app.config.components.mongo.mongoose.model("Address", AddressSchema)
  };
};

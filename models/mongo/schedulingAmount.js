const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const SchedulingAmount = new Schema({
  quantity: {type: Number, required: true},
  plan: {type: String},
  patient: {type: Schema.Types.ObjectId, ref: 'User', required: true},
  created: {type: Date, default: Date.now}
});

module.exports = app => {
  return {
    db: app.config.components.mongo.mongoose.model(
      'SchedulingAmount',
      SchedulingAmount
    )
  };
};

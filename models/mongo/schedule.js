const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const Schedule = new Schema({
  date: {type: Date, required: true},
  hour: {type: String, required: true},
  status: {
    type: String,
    default: 'aprovate',
    enum: ['canceled-doctor', 'canceled-patient', 'aprovate']
  },
  doctor: {type: Schema.Types.ObjectId, ref: 'User', required: true},
  patient: {type: Schema.Types.ObjectId, ref: 'User', required: true},
  created: {type: Date, default: Date.now}
});

module.exports = app => {
  return {
    db: app.config.components.mongo.mongoose.model('Schedule', Schedule)
  };
};

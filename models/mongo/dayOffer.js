const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const DayOffer = new Schema({
  hours: {type: [String], required: true},
  user: {type: Schema.Types.ObjectId, ref: 'User', required: true},
  weekDay: {type: Number, required: true}
});

module.exports = app => {
  return {
    db: app.config.components.mongo.mongoose.model('DayOffer', DayOffer)
  };
};

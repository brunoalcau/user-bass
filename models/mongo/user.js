const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  bcrypt = require('bcrypt'),
  saltRounds = 10;

const UserSchema = new Schema({
  email: {type: String, required: true, index: {unique: true}, lowercase: true},
  password: {type: String, required: false, select: false},
  name: {type: String, required: true},
  telephone: {type: String},
  accessToken: {type: String, select: false},
  type: {
    type: String,
    default: 'patient',
    enum: ['patient', 'patient-student', 'doctor', 'doctor-supervisor']
  },
  urlImage: {type: String},

  status: {
    type: String,
    default: 'aprovate-patient',
    enum: [
      'process-aprovate-patient-student',
      'process-aprovate-doctor-supervisor',
      'process-aprovate-doctor',
      'process-register-doctor',
      'aprovate-patient-student',
      'aprovate-patient',
      'aprovate-doctor',
      'aprovate-doctor-supervisor'
    ]
  }
});

UserSchema.pre('save', async function(next) {
  const user = this;

  try {
    const salt = bcrypt.genSaltSync();
    if (user.password) user.password = bcrypt.hashSync(user.password, salt);
    next();
  } catch (e) {
    throw new Error(e.message);
  }
});

UserSchema.methods.comparePassword = async function(candidatePassword) {
  const user = this;
  const isMatch = user.password
    ? await bcrypt.compare(candidatePassword, this.password)
    : false;
  return isMatch;
};

module.exports = app => {
  return {
    db: app.config.components.mongo.mongoose.model('User', UserSchema)
  };
};
